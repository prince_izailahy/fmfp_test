*** Settings ***
Library    SeleniumLibrary
    
*** Test Cases ***
Ouvrir navigateur
    Ouvrir google
    Set Browser Implicit wait    5 
    sleep    2
    CLose Browser
    
Go To linkedin
    Open Browser    ${URL_LINKEDIN}    ${NAVIGATEUR}
    Set Browser Implicit wait    10
    Click link    href="https://www.linkedin.com/login?fromSignIn=true&trk=guest_homepage-basic_nav-header-signin"
    sleep    2
    CLose Browser
    
Recheche google
    Ouvrir google
    #Set Browser Implicit wait    10
    Input Text    name=q    robot framework
    #Set Browser Implicit wait    10
    Press Keys    none    ENTER
    #sleep    2
    CLose Browser
  
*** Variable ***
${URL_GOOGLE}    https://google.com
${URL_LINKEDIN}    https://fr.linkedin.com
${NAVIGATEUR}    Chrome

*** Keywords ***
Ouvrir google
    Open Browser    ${URL_GOOGLE}    ${NAVIGATEUR}
    
    